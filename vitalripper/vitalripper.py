import sys
import re
import requests
import time
import json
import argparse
from pathlib import Path
from selenium import webdriver
from seleniumrequests import Chrome
from selenium.common.exceptions import NoSuchElementException
from xdg import XDG_DATA_HOME
from fpdf import FPDF
from PyPDF2 import PdfFileReader, PdfFileWriter
from PIL import Image


class PageNotFound(Exception):
    def __init__(self, request):
        self.request = request


class VSTPageGlyph:
    def __init__(self, l, r, t, b, c):
        self.l = l
        self.r = r
        self.t = t
        self.b = b
        self.c = c


class VSTGlyphBar:
    def __init__(self, glyphs):
        # TODO: make it a config parameter
        self._threshold = 11
        if type(glyphs) == list:
            self.glyphs = sorted(glyphs, key=lambda k: k.l)
        elif type(glyphs) == VSTPageGlyph:
            self.glyphs = [glyphs]

    def add_glyph(self, glyph: VSTPageGlyph):
        self.glyphs.append(glyph)
        self.glyphs = sorted(self.glyphs, key=lambda k: k.l)

    def fits_in(self, glyph: VSTPageGlyph):
        if glyph.t == self.top_of_line:
            if glyph.l - self.end_of_line < self._threshold:
                return True
        return False

    @property
    def width(self):
        return self.end_of_line - self.start_of_line

    @property
    def height(self):
        return self.bottom_of_line - self.top_of_line

    @property
    def words(self):
        return ''.join([glyph.c for glyph in self.glyphs])

    @property
    def start_of_line(self):
        return self.glyphs[0].l

    @property
    def end_of_line(self):
        # Technically end_of_line calculation is wrong, should re-sort the list for the highest ['r'], but
        # practically this solution should be goo enough, as all the glyphs are just one letter wide anyway
        # (apparently).
        return self.glyphs[-1].r

    @property
    def top_of_line(self):
        # Not really relevant, just take first element for calculations
        # As far as I've seen, the top values always match for all glyphs of the same t_values
        return self.glyphs[0].t

    @property
    def bottom_of_line(self):
        # Not really relevant, just take last element for calculations
        # As far as I've seen, the bottom values always match for all glyphs of the same t_values
        return self.glyphs[-1].b


class VSTPage:
    def __init__(self, sourcefile: Path, page_width, page_height, datafile_suffix=".vst-html-javascript"):
        self.glyphbars = []
        self.sourcefile = sourcefile
        with open(f"{sourcefile.with_suffix(datafile_suffix)}") as f:
            content = f.read()
            # There is some other VST trash around the actual json data, so we need to cut that off.
            # => cut off one of { and } on each side
            # Setup: "foobar { trash {json-content} more-trash } more-foobar"
            json_start_index = content.index('{', content.index('{') + 1)
            json_end_index = content.rindex('}', 0, content.rindex('}'))
            vst_json = json.loads(content[json_start_index:json_end_index + 1])

            # [l]eft, [r]ight, [t]op, [b]ottom coordinates (probably)
            vst_glyphs = [VSTPageGlyph(glyph['l'] / 100 * page_width, glyph['r'] / 100 * page_width,
                                       glyph['t'] / 100 * page_height, glyph['b'] / 100 * page_height,
                                       vst_json['words'][index])
                          for index, glyph in enumerate(vst_json['glyphs']['glyphs'])]
            t_diffs = {glyph.t for glyph in vst_glyphs}  # different lines

            self.chapter_title = vst_json['chapterTitle']

            for glyph_t in sorted(t_diffs):
                current_line_glyphs = [glyph for glyph in vst_glyphs if glyph.t == glyph_t]
                current_line_glyphs = sorted(current_line_glyphs, key=lambda k: k.l)
                current_glyphbar = None
                for current_glyph in current_line_glyphs:
                    if current_glyphbar is None:
                        current_glyphbar = VSTGlyphBar(current_glyph)
                    elif current_glyphbar.fits_in(current_glyph):
                        current_glyphbar.add_glyph(current_glyph)
                    else:
                        self.glyphbars.append(current_glyphbar)
                        current_glyphbar = VSTGlyphBar(current_glyph)
                # Add last glyphbar as well
                if current_glyphbar is not None:
                    self.glyphbars.append(current_glyphbar)


class VSTBook:
    def __init__(self, root_path, height, width):
        self._root_path = root_path
        self.height = height
        self.width = width
        self.pages = []

        # iterate over every file and add to pdf
        for file in sorted(root_path.glob('*.jpg')):
            self.pages.append(VSTPage(file, self.width, self.height))

    def to_pdf(self, output_file: Path):
        # set pdf units and width and height
        pdf = FPDF(unit="pt", format=[self.width, self.height])
        # avoid white/empty pages
        pdf.set_auto_page_break(False)
        # TODO: this should be added as configuration options
        font = 'FreeSans'
        font_size_calc_step = 0.1  # The lower, the better it'll get approximated but will also take longer
        pdf.add_font(font, '', '/usr/share/fonts/noto/NotoSans-Medium.ttf', uni=True)

        # Need to set font, else fpdf will crash/complain.
        pdf.set_font(font, size=font_size_calc_step)

        for vstpage in self.pages:
            pdf.add_page()
            for vstglyphbar in vstpage.glyphbars:
                # Setting to coordinates provided by glyph
                pdf.set_xy(vstglyphbar.start_of_line, vstglyphbar.top_of_line)
                # Reset font size to minimum
                font_size = font_size_calc_step
                # Important, also set the minimum font size, else get_string_width will calculate based on the previous
                # font size and usually fail.
                pdf.set_font(font, size=font_size)
                while pdf.get_string_width(vstglyphbar.words) < vstglyphbar.width:
                    pdf.set_font(font, size=font_size)
                    font_size += font_size_calc_step
                # subtract the step to reach the last font size that fit into the box
                pdf.set_font(font, size=font_size - font_size_calc_step)
                # DEBUG: change 4th param into 1 and disable pdf.image() call below to draw borders for the text
                # and see if it fits
                pdf.cell(vstglyphbar.width, vstglyphbar.height, vstglyphbar.words, 1)
            pdf.image(str(vstpage.sourcefile), 0, 0)
        # print out the pdf. This can take some time
        print("Compiling PDF. This may take several minutes. Please be patient.")
        pdf.output(output_file, "F")

        # adding bookmarks/outlines
        with open(output_file, 'rb') as f:
            pdfreader = PdfFileReader(f)
            pdfwriter = PdfFileWriter()
            current_bookmark = ''
            for number, page in enumerate(pdfreader.pages):
                pdfwriter.addPage(page)
                if current_bookmark != self.pages[number].chapter_title:
                    pdfwriter.addBookmark(self.pages[number].chapter_title, number)
                    current_bookmark = self.pages[number].chapter_title
            with open(output_file.with_suffix('.tmp-bookmarks'), 'wb') as tf:
                pdfwriter.write(tf)
            output_file.with_suffix('.tmp-bookmarks').replace(output_file)


def get_session(email, password):
    """
    Starts webdriver-driven browser session. A real browser is required, since the DRM is executed via javascript and
    only allows access to the book via javascript and enabled third-party cookies.
    """
    # TODO: add configuration to allow for different backends or just make firefox default
    # create a new Chrome session
    option = webdriver.ChromeOptions()
    option.add_argument(' --incognito')
    # driver = webdriver.Chrome(chrome_options=option)
    driver = Chrome()
    driver.implicitly_wait(30)

    # Get the login page and html
    login_url = 'https://evantage.gilmoreglobal.com/#/user/signin'
    driver.get(login_url)

    # email_elem = driver.find_element_by_id("email-field")
    # password_elem = driver.find_element_by_id("password-field")
    # email_elem.send_keys(email)
    # password_elem.send_keys(password)
    # driver.find_element_by_id("submit-btn").click()
    input("Successfully signed in? Then press enter.")
    return driver


def get_page(driver, page_nr, quality, root_path):
    # TODO: check if parameters in url can be reduced/removed
    jigsaw_frame_url = f"https://jigsaw.gilmoreglobal.com/books/{book}/cfi/{page_nr}!/4/4@0.00:0.00?jigsaw_brand=evantage&dps_on=false&xdm_e=https%3A%2F%2Fevantage.gilmoreglobal.com&xdm_c=default5266&xdm_p=1"

    driver.get(jigsaw_frame_url)
    driver.switch_to.frame(driver.find_element_by_id('epub-content'))
    pbk_page_src = driver.find_element_by_id('pbk-page').get_attribute('src')
    # replace pre-set quality in url with wanted quality
    # FIXME: this appears to not always work, sometimes the quality is pre-set to "undefined" apparently?
    pbk_page_src = re.sub(r"\d+$", str(quality), pbk_page_src)

    vst_html_javascript = driver.find_element_by_id('vst-html-javascript').get_attribute('innerHTML')
    # the pdf_ax_text element is also useful and contains the text of the page, probably for screen readers

    # Since we don't want to download the book-page-jpg via the browser functionality, we use the seleniumrequests
    # package, which shares the cookies from the webdriver session with the requests module, so we can download the
    # book-page-jpg using requests and save it on local filesystem ourselves.
    r = driver.request('get', pbk_page_src)
    if r.status_code == requests.codes.ok:
        prefix = f"page_{str(page_nr).zfill(4)}"
        with open(root_path / Path(f'{prefix}.jpg'), 'wb') as f:
            f.write(r.content)
        with open(root_path / Path(f'{prefix}.vst-html-javascript'), 'w') as f:
            f.write(vst_html_javascript)
    elif r.status_code == requests.codes.too_many_requests:
        # DRM also employs rate-limiting
        print("http 429, waiting and retrying...")
        time.sleep(10)
        get_page(driver, page_nr, quality, root_path)
    else:
        raise PageNotFound(r)


def convert_images_to_pdf(book, root_path: Path, output_dir: Path):
    # get pdf size from the first page
    first_page = Image.open(f"{sorted(root_path.glob('*.jpg'))[0]}")
    vstbook = VSTBook(root_path, first_page.size[1], first_page.size[0])
    vstbook.to_pdf(output_dir / Path(f'{book}.pdf'))


def download_book(email, password, book, mode, quality=2000):
    # get session
    # TODO: add configuration parameter to toggle between downloading and compiling a book
    #  (e.g. when post-processing files before compilation or for development and debugging)
    if mode in [0, 1]:
        driver = get_session(email, password)

    # TODO: add page selection as configuration parameter
    page = 0
    root_path = XDG_DATA_HOME / Path('vitalripper') / Path(f"{book}")
    output_dir = XDG_DATA_HOME / Path('vitalripper') / Path('compiled-books')

    # make the directory if it does not exist
    root_path.mkdir(parents=True, exist_ok=True)
    output_dir.mkdir(parents=True, exist_ok=True)

    # loop through and download all pages. Will stop once the next page doesn't exist
    while mode in [0, 1]:
        # print page info to keep user engaged
        if page % 10 == 0:
            print("Downloading page {}".format(page))

        # download image
        try:
            get_page(driver, page, quality, root_path)
            page += 1
        except (PageNotFound, NoSuchElementException) as e:
            # stop loop
            print("End of pages. Book is {} pages long".format(page))
            break

    if mode in [0, 2]:
        # Convert all images to pdf
        convert_images_to_pdf(book, root_path, output_dir)


def main():
    # dynaconf?
    parser = argparse.ArgumentParser(description='Script to download books from evantage.gilmoreglobal.com')
    parser.add_argument("email")
    parser.add_argument("password")  # should be file/env?
    parser.add_argument("book")
    parser.add_argument("--page", type=int)
    parser.add_argument("--no-download", action="store_true", type=bool)
    parser.add_argument("--no-compile", action="store_true", type=bool)


if __name__ == "__main__":
    if len(sys.argv) == 5:
        email = sys.argv[1]
        password = sys.argv[2]
        book = sys.argv[3]
        mode = int(sys.argv[4])
        download_book(email, password, book, mode)
    else:
        print("Usage: python ripper.py email password book_number mode[0=download+compile, 1=download, 2=compile]")
